import 'package:flutter/material.dart';

class App extends StatelessWidget{
  
  int count = 0;
  
  countOnClick(){
    count++;
  }
   
  Widget build(BuildContext context) {
    var appWidget = MaterialApp(
        home : Scaffold(
          appBar: AppBar(title : Text('This is top app bar')),
          body: Text('This is app body'),
          floatingActionButton : FloatingActionButton(
            child : Icon(Icons.add),
            onPressed : countOnClick
          )
        )
    );
    
    return appWidget;
  }
}