import 'package:flutter/material.dart';

void main(){
  var app = MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text('Test app')),
        body: Icon(Icons.ac_unit, color: Colors.lightBlueAccent, size: 24.0)
      )
  );
  runApp(app);
}