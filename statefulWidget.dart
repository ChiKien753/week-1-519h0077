import 'package:flutter/material.dart';

class App extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return AppState();
  }
}

class AppState extends State<App>{
  int count = 0;
  
  countOnClick(){
    count++;
    setState((){});
  }
   
  Widget build(BuildContext context) {
     var appWidget = MaterialApp(
        home : Scaffold(
          appBar: AppBar(title : Text('This is top app bar')),
          body: Text('This is app body, count: $count'),
          floatingActionButton : FloatingActionButton(
            child : Icon(Icons.add),
            onPressed : countOnClick
          )
        )
     );
    
    return appWidget;
  }
}