import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main(){
  Container(
    child: Text('Hello world'),
    decoration: BoxDecoration(
      shape: BoxShape.circle,
      color: Colors.yellow,
    ),
    margin: EdgeInsets.all(25.0),
    padding: EdgeInsets.all(40.0),
  );
}