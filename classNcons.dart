class Bike{
    String? model;
    String? engine;

    Bike(this.model, this.engine);

    @override
    toString(){
        return '$model : $engine';
    }
}

main(){
    Bike bike1 = Bike('ZX10r', 'I4');
    Bike bike2 = Bike('R1', 'CP4');
    Bike bike3 = Bike('RSV4', 'V4');

    print(bike1);
    print(bike2);
    print(bike3);
}