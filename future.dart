import 'dart:async';

void main() async {
  get('https://www.google.com').then((value) {
    print(value);
  });
}

Future<String> get(url){
  return Future.delayed(Duration(seconds: 5), (){
    return 'This application has been delayed for 5s';
  });
}